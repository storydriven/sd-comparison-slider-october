(function($) {

    $.fn.twentytwenty = function(options) {
        var options = $.extend({ default_offset_pct: 0.5, orientation: 'horizontal' }, options);
        return this.each(function() {

            var sliderPct = options.default_offset_pct;
            var container = $(this);
            var sliderOrientation = options.orientation;
            var beforeDirection = (sliderOrientation === 'vertical') ? 'down' : 'left';
            var afterDirection = (sliderOrientation === 'vertical') ? 'up' : 'right';


            container.wrap("<div class='twentytwenty-wrapper twentytwenty-" + sliderOrientation + "'></div>");
            var beforeImg = container.find("img:first");
            var afterImg = container.find("img:last");
            var slider = container.find(".twentytwenty-handle");
            container.addClass("twentytwenty-container");
            beforeImg.addClass("twentytwenty-before");
            afterImg.addClass("twentytwenty-after");

            var overlay = container.find(".twentytwenty-overlay");

            var calcOffset = function(dimensionPct) {
                var w = beforeImg.width();
                var h = beforeImg.height();
                return {
                    w: w + "px",
                    h: h + "px",
                    cw: (dimensionPct * w) + "px",
                    ch: (dimensionPct * h) + "px"
                };
            };

            var adjustContainer = function(offset) {
                if (sliderOrientation === 'vertical') {
                    beforeImg.css("clip", "rect(0," + offset.w + "," + offset.ch + ",0)");
                } else {
                    beforeImg.css("clip", "rect(0," + offset.cw + "," + offset.h + ",0)");
                    if (offset.cw.replace("px", "") < 60) {
                        $('.twentytwenty-before-label').css('opacity', '0');
                    } else {
                        $('.twentytwenty-before-label').css('opacity', '1');
                    }

                    if (offset.cw.replace("px", "") > $(window).width() - 60) {
                        $('.twentytwenty-after-label').css('opacity', '0');
                    } else {
                        $('.twentytwenty-after-label').css('opacity', '1');
                    }
                }
                container.css("height", offset.h);
            };

            var adjustSlider = function(pct) {
                var offset = calcOffset(pct);
                slider.css((sliderOrientation === "vertical") ? "top" : "left", (sliderOrientation === "vertical") ? offset.ch : offset.cw);
                adjustContainer(offset);
            }

            $(window).on("resize.twentytwenty", function(e) {
                adjustSlider(sliderPct);
            });

            var offsetX = 0;
            var offsetY = 0;
            var imgWidth = 0;
            var imgHeight = 0;

            slider.on("movestart", function(e) {
                if (((e.distX > e.distY && e.distX < -e.distY) || (e.distX < e.distY && e.distX > -e.distY)) && sliderOrientation !== 'vertical') {
                    e.preventDefault();
                } else if (((e.distX < e.distY && e.distX < -e.distY) || (e.distX > e.distY && e.distX > -e.distY)) && sliderOrientation === 'vertical') {
                    e.preventDefault();
                }
                container.addClass("active");
                offsetX = container.offset().left;
                offsetY = container.offset().top;
                imgWidth = beforeImg.width();
                imgHeight = beforeImg.height();
            });

            slider.on("moveend", function(e) {
                container.removeClass("active");
            });

            slider.on("move", function(e) {
                if (container.hasClass("active")) {
                    sliderPct = (sliderOrientation === 'vertical') ? (e.pageY - offsetY) / imgHeight : (e.pageX - offsetX) / imgWidth;
                    if (sliderPct < 0) {
                        sliderPct = 0;
                    }
                    if (sliderPct > 1) {
                        sliderPct = 1;
                    }
                    adjustSlider(sliderPct);
                }
            });

            container.find("img").on("mousedown", function(event) {
                event.preventDefault();
            });

            var handleLock = false;

            $('.twentytwenty-handle').mouseenter(function() {
                if (handleLock == false) {
                    $('.twentytwenty-tool-tip').stop().fadeIn();
                }
            });

            $('.twentytwenty-handle').mouseleave(function() {
                $('.twentytwenty-tool-tip').stop().fadeOut();
            });


            $('.twentytwenty-handle').on("mousedown", function(event) {
                $(this).css('background-color', '#333f48');
                $('.twentytwenty-tool-tip').css('display', 'none');
                handleLock = true;
            });
            $('.twentytwenty-handle').on("mouseup", function(event) {
                $(this).css('background-color', '#a69f88');
                $('.twentytwenty-tool-tip').fadeIn();
                handleLock = false;
            });
            $(window).trigger("resize.twentytwenty");
        });
    };

})(jQuery);

$(window).on('load', function() {
    $(".twentytwenty-container[data-orientation!='vertical']").twentytwenty({
        default_offset_pct: 0.5
    });
    $(".twentytwenty-container[data-orientation='vertical']").twentytwenty({
        default_offset_pct: 0.5,
        orientation: 'vertical'
    });
});

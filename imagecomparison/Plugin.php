<?php namespace Storydriven\ImageComparison;

/**
 * The plugin.php file (called the plugin initialization script) defines the plugin information class.
 */

use System\Classes\PluginBase;
use Backend;

class Plugin extends PluginBase
{

    public function pluginDetails()
    {
        return [
            'name'        => 'Storydriven Image Comparison',
            'description' => 'moveDivisor to compare images',
            'author'      => 'Kelvin Xu',
            'icon'        => 'icon-leaf'
        ];
    }

    public function registerComponents()
    {
        return [
            '\Storydriven\ImageComparison\Components\Comparison' => 'comparison'
        ];
    }
}

<?php namespace Storydriven\ImageComparison\Components;

use Cms\Classes\ComponentBase;
use ApplicationException;

class Comparison extends ComponentBase
{
    public $image1;
    public $image2;
    public $beforeLabel;
    public $afterLabel;
    public function componentDetails()
    {
        return [
        'name'        => 'Image Comparison',
        'description' => 'moveDivisor to compare images'
        ];
    }

    public function onRun() {
        $this->addCss('/plugins/storydriven/imagecomparison/assets/twentytwenty.css');
        $this->addJs('/plugins/storydriven/imagecomparison/assets/jquery.event.move.js');
        $this->addJs('/plugins/storydriven/imagecomparison/assets/jquery.twentytwenty.js');
    }
    public function onRender() {
        $this->image1 = $this->property('image1');
        $this->image2 = $this->property('image2');
        $this->beforeLabel = $this->property('beforeLabel');
        $this->afterLabel = $this->property('afterLabel');
    }
}